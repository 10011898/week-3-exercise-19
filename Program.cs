﻿using System;

namespace week_3_exercise_19
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 1;
            var num2 = 2;
            var num3 = 3;
            var num4 = 4;
            var num5 = 5;

            Console.WriteLine($"The value of num1 = {num1}");
            Console.WriteLine($"The value of num2 = {num2}");
            Console.WriteLine($"The value of num3 = {num3}");
            Console.WriteLine($"The value of num4 = {num4}");
            Console.WriteLine($"The value of num5 = {num5}");

            var num6  = 6;
            var num7  = 7;
            var num8  = 8;
            var num9  = 9;
            var num10 = 10;
            var num11 = 11;

            Console.WriteLine($"The value of num6 + num7 = {num6+num7}");
            Console.WriteLine($"The value of num8 + num9 = {num8+num9}");
            Console.WriteLine($"The value of num10 + num11 = {num10+num11}");

            var num12 = 12;
            var num13 = 13;
            var num14 = 14;
            var num15 = 15;

            Console.WriteLine($"The value of num12 += num13 = {num12+=num13}");
            Console.WriteLine($"The value of num14 += num15 = {num14+=num15}");

            // var strVar = "I am thikning...";
            // Console.WriteLine(strVar * 3);
            // It crashes.
        }
    }
}
